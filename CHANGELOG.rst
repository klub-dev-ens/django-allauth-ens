*****
1.1.3
*****

- Fix: translation was not included in the package


*****
1.1.2
*****

- Options added to ``install_longterm`` command to preserve usernames and source
  clipper identifiers from various sources.  !18

*****
1.1.1
*****

- Pin python-cas version to make Clipper provider work.  !17

*****
1.1.0
*****

- Add long-term support for Clipper (see README for instructions on setup/update)
- Highlight Clipper provider in login screens (see related settings in README)
- Vendorize some static files (JS)

*******
1.0.0b2
*******

- First official release.

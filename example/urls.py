from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth.decorators import login_required, permission_required
from django.views.generic import RedirectView

import debug_toolbar
from allauth_ens.views import capture_login, capture_logout

from app import views

urlpatterns = [
    # Catch admin login/logout views.
    url(r'^admin/login/', capture_login),
    url(r'^admin/logout/', capture_logout),

    # Admin urls include comes after.
    url(r'^admin/', admin.site.urls),

    # Base views with different required permissions.
    url(r'^view/', views.HomeView.as_view(),
        name='view'),
    url(r'^user/', login_required()(views.HomeView.as_view()),
        name='user-view'),
    url(r'^root/', permission_required('foo.perm')(views.HomeView.as_view()),
        name='root-view'),

    # Authens urls (handle login/logout views).
    url(r'^account/', include('allauth_ens.urls')),


    # (Redirect from /)
    url(r'^$', RedirectView.as_view(url='/view/'),
        name='home'),
]

urlpatterns += [url(r'^__debug__/', include(debug_toolbar.urls))]

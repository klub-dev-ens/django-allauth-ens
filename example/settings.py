import os

import django

BASE_DIR = os.path.dirname(os.path.abspath(__file__))


SECRET_KEY = 'iamaplop'

DEBUG = True

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.messages',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.staticfiles',

    'debug_toolbar',
    'widget_tweaks',

    'allauth_ens',
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    'allauth_cas',

    'allauth.socialaccount.providers.facebook',
    'allauth.socialaccount.providers.google',
    'allauth_ens.providers.clipper',

    'app',
]

_MIDDLEWARES = [
    'django.middleware.common.CommonMiddleware',
    'debug_toolbar.middleware.DebugToolbarMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.sites.middleware.CurrentSiteMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
]

if django.VERSION >= (1, 10):
    MIDDLEWARE = _MIDDLEWARES
else:
    MIDDLEWARE_CLASSES = _MIDDLEWARES

ROOT_URLCONF = 'urls'

SITE_ID = 1

WSGI_APPLICATION = 'wsgi.application'

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': (
            'django.contrib.auth.password_validation'
            '.UserAttributeSimilarityValidator'
        ),
    },
    {
        'NAME': (
            'django.contrib.auth.password_validation'
            '.MinimumLengthValidator'
        ),
    },
    {
        'NAME': (
            'django.contrib.auth.password_validation'
            '.CommonPasswordValidator'
        ),
    },
    {
        'NAME': (
            'django.contrib.auth.password_validation'
            '.NumericPasswordValidator'
        ),
    },
]

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Europe/Paris'

USE_I18N = True

USE_L10N = True

USE_TZ = True

STATIC_URL = '/static/'

DEBUG_TOOLBAR_CONFIG = {
    'SHOW_TOOLBAR_CALLBACK': lambda r: True,
}

AUTHENTICATION_BACKENDS = [
    'django.contrib.auth.backends.ModelBackend',
    'allauth.account.auth_backends.AuthenticationBackend',
]

LOGIN_URL = '/account/login/'
LOGIN_REDIRECT_URL = 'user-view'
ACCOUNT_LOGOUT_REDIRECT_URL = 'view'

SOCIALACCOUNT_QUERY_EMAIL = True

# allauth settings

ACCOUNT_ADAPTER = 'adapter.AccountAdapter'
ACCOUNT_AUTHENTICATED_LOGIN_REDIRECTS = False
ACCOUNT_AUTHENTICATION_METHOD = 'username_email'

SOCIALACCOUNT_ADAPTER = 'adapter.SocialAccountAdapter'

# allauth_ens settings

ACCOUNT_HOME_URL = 'home'
ACCOUNT_DETAILS_URL = '/my-account/'

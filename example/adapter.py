# -*- coding: utf-8 -*-
from allauth.account.adapter import DefaultAccountAdapter
from allauth_ens.adapter import LongTermClipperAccountAdapter
from allauth.socialaccount.adapter import DefaultSocialAccountAdapter

class AccountAdapter(DefaultAccountAdapter):
    pass


class SocialAccountAdapter(LongTermClipperAccountAdapter):
    
    def get_username(self, clipper, data):
        """
        Exception-free version of get_username, so that it works even outside of
        the ENS (if no access to LDAP server)
        """
        return "{}@{}".format(clipper, data.get('entrance_year', '00'))

from django.apps import AppConfig
from django.db.models.signals import post_migrate
from django.utils.module_loading import import_string

from allauth.socialaccount.providers import registry


def setup_site(sender, **kwargs):
    from django.contrib.sites.models import Site
    Site.objects.filter(id=1).update(name="Demo Site", domain="localhost")


def setup_users(sender, **kwargs):
    from django.contrib.auth import get_user_model
    User = get_user_model()

    root, r_created = User.objects.get_or_create(username='root', defaults={
        'email': 'root@mail.net',
        'is_staff': True,
        'is_superuser': True,
    })
    if r_created:
        root.set_password('root')
        root.save()
        print('Superuser created - Credentials: root:root')

    user, u_created = User.objects.get_or_create(username='user', defaults={
        'email': 'user@mail.net',
    })
    if u_created:
        user.set_password('user')
        user.save()
        print('User created - Credentials: user:user')


def setup_dummy_social(sender, **kwargs):
    from django.contrib.sites.models import Site

    from allauth.socialaccount.models import SocialApp

    need_credentials = [
        'allauth.socialaccount.providers.oauth.provider.OAuthProvider',
        'allauth.socialaccount.providers.oauth2.provider.OAuth2Provider',
    ]

    classes = tuple([import_string(cls_path) for cls_path in need_credentials])
    site = Site.objects.get_current()

    dummy_installed = []

    for provider in registry.get_list():
        if isinstance(provider, classes):
            try:
                SocialApp.objects.get(provider=provider.id, sites=site)
            except SocialApp.DoesNotExist:
                app = SocialApp.objects.create(
                    provider=provider.id,
                    secret='secret',
                    client_id='client-id',
                    name='Dummy %s app' % provider.id,
                )
                app.sites.add(site)
                dummy_installed.append(provider.id)

    if dummy_installed:
        print(
            "Dummy application credentials installed for: %s.\n"
            "Authentication via these providers will not work until you "
            "configure proper credentials via the Django admin (`SocialApp` "
            "model)."
            % ', '.join(sorted(dummy_installed))
        )


class BasicAppConfig(AppConfig):
    name = 'app'

    def ready(self):
        post_migrate.connect(setup_site, sender=self)
        post_migrate.connect(setup_dummy_social, sender=self)
        post_migrate.connect(setup_users, sender=self)

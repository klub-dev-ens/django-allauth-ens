*****
Setup
*****

Clone the repository and go to the directory containing this file.

If it is the first time you start the example website, run these commands::

  # Create a virtual env.
  $ virtualenv -p python3 venv
  $ . venv/bin/activate

  # Install dependencies (django-allauth-ens is installed from local copy).
  $ pip install -r requirements.txt

  # Initialize the database (sqlite).
  $ ./manage.py migrate

Start the server with::

  $ ./manage.py runserver

Then, open your browser at `<http://localhost:8000/>`_.


*****
Usage
*****

Pre-existing users
==================

You can try to login using one of the existing users:

* a "normal" user (username: 'user', password: 'user')

* a superuser (username: 'root', password: 'root')

Auth Providers
==============

Facebook and Google are activated but they won't work unless you provide valid
API credentials in the Django Admin at
`<http://localhost:8060/admin/socialaccount/socialapp/>`_.

Clipper is available (which requires valid credentials). For auto-signup, the
new user is populated with the clipper identifier as username (plus a suffix if
not available) and ``<clipper id>@clipper.ens.fr`` as email.

Adapters
========

``ACCOUNT_ADAPTER`` and ``SOCIALACCOUNT_ADAPTER`` can be customized in
``adapter.py``.

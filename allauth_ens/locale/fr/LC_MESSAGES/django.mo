��    4      �  G   \      x  Y   y  �   �  �   x  ^   $  S   �  U   �  ,   -  ~   Z  b   �  `   <  J   �  #   �  �   	  <   �	  f   �	  �   C
  �   �
  �   o  g     '   {     �     �  
   �     �     �     �               0     A     T     e     u     �     �     �     �     �     �     �     �                    "     *     9  #   @     d  #   �     �  J  �  o     �   �  �   6  i   �  L   e  M   �  8      �   9  q   �  d   R  I   �        �   "  \   �  x   ,  �   �  �   P  �   8  o   �  .   k     �     �     �     �     �  	             ,     B     R     g     ~     �     �     �     �     �  !   �  '     	   =     G     f  	        �     �     �     �  *   �  $   �  '      $   H               
              &   "   !         	   0   *                        ,              +   .                                $         1           4      )         2   '         %          #            3          /   (       -                        
          Authentication failed. Please check your credentials and try again.
           
          Your are authenticated as %(user_str)s, but are not authorized to access
          this page. Would you like to login to a different account ?
           
      The password reset link was invalid, possibly because it has already been used.
      Please request a <a href="%(passwd_reset_url)s">new password reset</a>.
       
      You can sign in to your account using any of the following third party accounts:
       
      You currently have no third party accounts connected to this account.
       
    An error occured while attempting to login via your social network account.
     
    Are you sure you want to sign out?
     
    Forgotten your password? Enter your e-mail address below, and we'll send
    you an e-mail allowing you to reset it.
     
    Please confirm that <b>%(email)s</b> is an e-mail address for user
    %(user_display)s.
     
    Please sign in with one of your existing third party accounts, or with the form below.
     
    The following e-mail addresses are associated with your account:
     
    This account is inactive.
     
    This e-mail confirmation link expired or is invalid.<br>
    Please <a href="%(email_url)s">issue a new e-mail confirmation request</a>.
     
    We are sorry, but the sign up is currently closed.
     
    We have sent you an e-mail. Please contact us if you do not receive it within a few minutes.
     
    You are about to use your %(provider_name)s account to login.
    As a final step, please complete the following form:
     
    You currently do not have any e-mail address set up. You should really
    add an e-mail address so you can receive notifications, reset your
    password, etc.
     
    You decided to cancel logging into our site using one of your existing accounts. If this was a mistake, please proceed to <a href="%(login_url)s">sign in</a>. 
    Your account does not have a password yet. Add one to authenticate without
    third parties.
     
    Your password is now changed.
     Account Connections Account Inactive Add E-mail Already have an account? Change Password Confirm Confirm E-mail Address Connect with Clipper E-mail Addresses ENS Authentication Forgot Password? Login Cancelled Login Failure Make primary Not Connected Other choices Other ways to sign in/sign up Password Reset Re-send verification Remove Reset Password Set Password Sign In Sign Out Sign Up Sign Up Closed Signup This email address is not verified. This email address is verified. This is your primary email address. Your password is now changed. Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-06-24 20:10+0200
PO-Revision-Date: 2019-06-25 10:35+0200
Last-Translator: 
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 2.2.1
 
          L'authentification a échoué. Merci de vérifier vos identifiants et essayer à nouveau.
           
          Vous êtes identifié comme %(user_str)s, mais vous n'être pas autorisé à accéder
          à cette page. Voulez-vous essayer avec un compte différent ?
           
    Le lien de réinitialisation de mot de passe est invalide, possiblement parce qu'il a déjà été utilisé.
    Merci de faire une <a href="%(passwd_reset_url)s">nouvelle demande</a>.
       
    Vous pouvez vous connecter à votre compte en utilisant n'importe lequel de ces comptes tiers :
     
    Vous n'avez actuellement aucun compte tiers associé à ce compte.
     
    Une erreur s'est produite lors de la connexion via le site externe.
     
    Êtes-vous sûr de vouloir vous déconnecter ?
     
    Vous avez oublié votre mot de passe ? Entrez votre adresse e-mail ci-dessous, et
    nous vous enverrons un e-mail qui vous permettra de le réinitialiser.
     
    Merci de confirmer que <b>%(email)s</b> est une adresse e-mail pour l'utilisateur
    %(user_display)s.
     
    Connectez-vous avec l'un de vos comptes tiers existants, ou avec le formulaire ci-dessous.
     
    Les adresses mails suivantes sont associées à votre compte :
     
    Ce compte est inactif.
     
    Ce lien de confirmation d'e-mail a expiré ou est invalide.<br>
    Merci de <a href="%(email_url)s">soumettre une nouvelle requête de confirmation d'e-mail</a>.
     
    Nous sommes désolés, mais la création de compte est actuellement désactivée.
      
    Nous vous avons envoyé un e-mail. Merci de nous contacter si vous ne l'avez pas reçu d'ici quelques minutes.
     
    Vous êtes sur le point d'utiliser votre compte %(provider_name)s pour vous connecter
    Pour finaliser la procédure, merci de remplir le formulaire suivant :
     
    Vous n'avez actuellement aucune adresse e-mail associée à votre compte.
    Vous devriez vraiment ajouter une adresse e-mail afin que vous puissiez recevoir
    des notifications, réinitialiser votre mot de passe, etc.
     
    Vous avez décidé d'annuler la connexion à notre site via un de vos comptes existants. Si cela était une erreur, vous pouvez revenir sur <a href="%(login_url)s">la page de connexion</a>. 
    Votre compte n'a pas encore de mot de passe. Ajoutez-en un pour vous connecter
    sans compte tiers.
     
    Votre mot de passe a été modifié.
     Méthodes de connexion Compte inactif Ajouter un e-mail Vous avez déjà un compte ? Changer le mot de passe Confirmer Confirmer l'adresse e-mail Connexion via Clipper Adresses e-mail Connexion pour l'ENS Mot de passe oublié ? Connexion annulée Échec de la connexion Rendre principale Non connecté Autres choix Autres méthodes de connexion Réinitialisation du mot de passe Ré-envoyer le message de vérification Supprimer Réinitialiser le mot de passe Définir le mot de passe Connexion Déconnexion Nouveau compte Création de compte fermée Nouveau compte Cette adresse e-mail n'est pas vérifiée. Cette adresse e-mail est vérifiée. Ceci est votre adresse e-mail primaire. Votre mot de passe a été modifié. 